

var videoWidget = function() { // base config for video	
	this.uid = 'video_widget';
	this.name = "Video";
	
		
	this.dynamicHeight = true;
	this.properties = {
		lineItems: [], //array of lineitem classes
	};
	
	this.position = { 
        x: 0, // % based positioning relative to a 1920x1080 resolution -> scaled
        y: 0,
        width: 150,
        height: 150,
        dynamicHeight: true  
    };
	
	this.templateurl = "widgets/video/templates/video.template.html";
	 
};
