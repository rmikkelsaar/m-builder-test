(function() {

var _widgetService = function($log, $rootScope) {
    
    
    var WidgetService = function() {
        this.position_class = {
            'margin-left': '0%',
            'margin-top': '0%',
            'z-index': 0,
            width: '150px', // Default widget width // Bound to positional object
            height: '110px' // Default widget height // Bound to positional object
        };
        this.bk_binding = $rootScope.bk_binding;
        this.subWidgets = [];
        this.parentWidget = null;
    };

    
    
    /**
     * inherit the parent class to the specific widget
     */
    WidgetService.prototype.inherit = function( widget, uid ) {
        this.uid = uid;
        
        // Global position for all widgets
        // All Positional parameters will not be done in pixels but relative pixels to scale of stage
        var propObject = [
            {
                name: "Position",
                id: "position",
                data: { 
                    'x': 0,
                    'y': 0,
                    'z': 0,
                    'width': 0, 
                    'height': 0 
                }, // Bound directly to position properties
                propType: "position"
            },
            {
                name: "Add Sub Widget",
                id: "widget_selection",
                data: [],
                propType: 'widgetSelection'  
            }   
        ]
        
        _.extend(this, widget); // inject properties of widgets into parent class
        this.properties = this.properties.concat(propObject);
        
    };
    
    /**
     * AddSubWidget
     */
    WidgetService.prototype.AddSubWidget = function(widget, uid) {
        widget.parentWidget = this;
        this.subWidgets.push(widget);    
    };
    
    WidgetService.prototype.getProperty = function(id) {
        var property = null;
        for( var i in this.properties ) {
            if( this.properties[i].id == id ) {
                property = this.properties[i];
                break;
            }
        }
        return property;    
    }
        
    
    
    /**
     * updatePosition
     * Updates the position of the widget on the stage
     * @param {string}  Key value of property
     * @param {int}  Integer value of property
     */
    WidgetService.prototype.updatePosition = function(prop, val) {
        if( prop == 'x' ) {
            this.position_class['margin-left'] = val + '%';
            this.getProperty('position').data.x = val;
        } else if( prop == 'y' ) {
            this.position_class['margin-top'] = val + '%';
            this.getProperty('position').data.y = val;
        } else if( prop == 'z' ) {
            this.position_class['z-index'] = val;
            this.getProperty('position').data.z = val;
        } else if( prop == 'width' || prop == 'height' ) {
            this.position_class[prop] = val + 'px';
            this.getProperty('position').data[prop] = val;
        }
    };
    
    return WidgetService;

    
}

angular
    .module('mBuilderApp')
    .service('widgetService', ['$log', '$rootScope', _widgetService ]);

})();