

var linelistWidget = function() { // base config for linelist	
	var self = this;
	this.name = "LineList";
	
	/**
	 * Dynamic array of configurable properties specific to widgets
	 * @param name: Name of property to configure
	 * @param data: the data to populate the configuration
	 * @param propType: describes how the main context panel describes the property configuration 
	 */
	this.properties = [
		{
			name: "Server Name",
			id: "server_name",
			data: "awesome_bk_binding",
			propType: "text"
		},
		{
			name: "Template Selector",
			id: "template_selector",
			data: [ 
				{ 
					name: "Panel Template",
					url: "widgets/linelist/templates/panel.template.html"					
				},
				{
					name: "Other Template",
					url: "widgets/linelist/templates/other.template.html"
				} 
			],
			propType: "dropdown"
		},
		{
			name: "Display Name",
			id: "display_name",
			data: "LineList Widget!",
			propType: "text"
		}	
	];
	
	this.properties.forEach(function(o) {
		if( o.name == "Template Selector" ) {
			self.selectedTemplate = o.data[0].url;
		}
	});
		
	// Persistable data from widgets 
	this.templateurl = "widgets/linelist/templates/panel.template.html";	
	this.dynamicHeight = true;	
	this.data_binding = {
		lineItems: [
			17.99,
			19.99,
			21.99
		]
	};
};

// Data Binding -> Retrieve from globally loaded xml data that will be periodically loaded from server
linelistWidget.prototype.bindData = function() {};
linelistWidget.prototype.LineItem = function() { // Sub class of linelistWidget
	// Will bind to parent class Databinding
	this.text = "Temp text",
	this.subitems = [ 'subitem1', 'subitem2' ];
};