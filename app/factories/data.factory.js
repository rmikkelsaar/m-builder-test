/**
 * Get XML data and parse array of products into global object of scope
 */
 
(function() {
    
    
var bkFactory = function($http, $q) {    
    
    var DataFactory = function() {
        this.getXmlData();
    };
    
    DataFactory.prototype.getXmlData = function() {            
        $http.get('lib/assets/menuboardv2.xml')
        .then(function(data) {
            var x2js = new X2JS();
            var jsonObj = x2js.xml_str2json( data.data );
            window.localStorage.setItem('bk_data', JSON.stringify( jsonObj.price_book.products.product) );
            console.log('got xml data');
        })
        .catch(function(e) {
            console.log('Error gettign xml data', e);
        });
    };    
    
    /**
     * getAllProducts
     */
    DataFactory.prototype.getAllProducts = function() {
        return JSON.parse(window.localStorage.getItem('bk_data'));
    };
    
    DataFactory.prototype.getMix = function(mix_value) {
        var products = this.getAllProducts();
        var mixValues = [];
        products.forEach(function(item) { // Loop over all items
            for( var i in item.attributes.attribute ) { // Loop until we find mix attribute, then break
                var attr = item.attributes.attribute[i];
                if( attr._name == "mix" ) {
                    if( attr.__text == mix_value ) {
                        mixValues.push(item);
                    }
                    break;
                }
            }
        });
        return mixValues;
    };
    
    
    
    
    
    return new DataFactory(); 
};

    
    

angular
    .module('mBuilderApp')
    .factory('bkFactory', ['$http', '$q', bkFactory]);
    
    
})();