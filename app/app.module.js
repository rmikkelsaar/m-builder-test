'use strict';

/**
 * @ngdoc overview
 * @name mbuilderTestApp
 * @description
 * # mbuilderTestApp
 *
 * Main module of the application.
 */
angular
  .module('mBuilderApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'modules/builder/builder.view.html',
        controller: 'builderCtrl',
        controllerAs: 'builder'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .constant('_', window._ ) // lodash constant
  .run(function($rootScope, bkFactory) {
      $rootScope.bk_binding = bkFactory;   
  });
