/* global _ */
'use strict';


/**
 * @ngdoc function
 * @name mbuilderTestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mbuilderTestApp
 */
var builderController = function ($log, $rootScope, WidgetService ) {
    var self = this;
    
    // this.selectedTheme = new ThemeClass(themeObject);
    this.selectedContext = null;
    
    this.Available_widgets = { // Assume already loaded
        'linelist': {
            Create: linelistWidget, // Assign instance to function property of widget
            name: "LineList"
        },
        'video': {
            Create: videoWidget, // Assign instance to function property of widget
            name: "Video"
        }
    };
    
    // Initialize Stage
    this.stage = {
        widgets: []
        // theme: self.selectedTheme
    };
    
    /**
     * addWidget
     * @param {string} Widget type
     */
    this.addWidget = function(Widget, parent) {
        $log.info('addWidget click', Widget);
        var uid = self.getNewUid();
        var widgetClass = new WidgetService();
        if( !parent ) { // Root widget
            widgetClass.inherit(new Widget.Create(),  uid);
            self.stage.widgets.push( widgetClass );
        } else {
            widgetClass.inherit(new Widget.Create(), uid);
            parent.AddSubWidget(widgetClass);
        }     
        
    };
    
    /**
     * Removes a widget from the stage model
     */
    this.removeWidget = function(widget) {
        var index = self.stage.widgets.indexOf(widget);
        self.stage.widgets.splice(index, 1);
        self.selectedContext = null;
    };
    
    /**
     * selectWidget
     */
    this.selectWidget = function(widget) {
        // test
        var bakedGoods = widget.bk_binding.getMix('Baked Goods');
        
        self.selectedContext = widget;
        this.justAdded = true;
        setTimeout(function() {
            self.justAdded = false;
        }, 50);
    };
    
    this.getNewUid = function() {
        if( self.stage.widgets.length == 0 ) {
            return 1;
        } else {
            return self.stage.widgets.slice(-1)[0].uid + 1;    
        }        
    };  
    
    this.onPositionUpdate = function(prop, val) {
        self.selectedContext.updatePosition(prop, val);       
    }; 
    
    
    this.stageClick = function() {
        if( this.selectedContext != null && !this.justAdded ) {
            this.selectedContext = null;
        }
    };
    
};


angular
    .module('mBuilderApp')
    .controller('builderCtrl', ['$log', '$rootScope', 'widgetService', builderController]);